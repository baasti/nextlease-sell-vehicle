module.exports = {
  content: [
    "./src/**/*.{html,js,jsx,ts,tsx}", "./node_modules/flowbite/**/*.js", './node_modules/tw-elements/dist/js/**/*.js',
  ],
  theme: {
    screens: {
      sm: '280px',
      md: '540px',
      lg: '768px',
      xl: '1024px',
      xxl: '1440',
    },
    fontSize: {
      'xs': '.75rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    },
    colors: {
      'blue': '#1fb6ff',
      'purple': '#7e5bef',
      'pink': '#ff49db',
      'orange': '#ff7849',
      'green': '#50866C',
      'yellow': '#ffc82c',
      'gray-dark': 'rgb(82, 82, 82)',
      'gray': '#8492a6',
      'gray-light': '#e5e7eb',
      'gray-super-light': '#f0f0f0',
      'light-grey': '#d1d5db40',
      'dark-green': '#1E3737',  
      'white': '#ffffff',
      'white-light': 'rgba(255, 255, 255, 0.5)',
      'white-md': 'rgba(255, 255, 255, 0.95)',
      'black': '#000',
      'bluegreen': '#51b8a4'
    },
    fontFamily: {
      sans: ['Inter', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      
    },
    
  },
  plugins: [
    require('tw-elements/dist/plugin'),
    require('flowbite/plugin')
  ],
}
