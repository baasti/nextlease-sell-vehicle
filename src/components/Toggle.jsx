import { useState } from "react";
import { Switch } from "@headlessui/react";

export default function Toggle() {
  const [isEnabled, setIsEnabled] = useState(false);

  return (
    <div className="button mt-4">
      <div>
        <Switch checked={isEnabled} onChange={setIsEnabled}>
          <span className="bg-white rounded shadow-md p-0.5 h-12 w-56 flex">
            <span
              className={`block text-white p-3 text-center text-sm justify-center h-full w-1/2 rounded transition duration-500 ease-in-out transform ${
                isEnabled ? "bg-green translate-x-full" : "bg-green"
              }`}
            >
                {isEnabled ? 'Inbyte' : 'Försäljning'}
            </span>
            <span className="p-3 pl-9 text-sm">Inbyte</span>
          </span>
        </Switch>
      </div>
    </div>
  );
}
