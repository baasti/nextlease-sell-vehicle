import React, { useState } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";



function ReactDatepicker() {
    const [startDate, setStartDate] = useState(new Date());

    return (
        <div className="container">
            <label className="block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2" for="grid-reg-nr">
                Miltal vid senaste service
            </label>
            <DatePicker className="appearance-none' block w-1/3 shadow-md border border-gray-super-light bg-gray-super-light rounded py-3 px-4 mb-3 leading-tight focus:outline-none 
                            focus:bg-white" id="grid-first-name" selected={startDate} onChange={(date) => setStartDate(date)} />
        </div>
    );
};

export default ReactDatepicker;
