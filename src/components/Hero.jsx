import React from 'react'
import hero from '../images/porsche-taycan.png'

function Hero() {
    return (
        <div className='w-full relative'>
            <div className='w-full h-full bg-dark-green top-0 left-0 absolute opacity-10'></div>
            <div className='w-full h-full flex flex-col absolute top-0 left-0 justify-center items-start ml-8 md:ml-14 md:mt-14'>
                <div className='text-white md:text-4xl'>Vi köper din bil!</div>
                <div className='text-white md:text-lg md:pl-12'>Smidigt & tryggt.</div>
            </div>
            <img src={hero} alt="" className='w-full' />

        </div>
    )
}

export default Hero;


