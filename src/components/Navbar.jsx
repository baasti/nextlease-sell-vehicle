import React from 'react'


function Navbar() {
  return (
    <div className="shadow-md w-full bg-dark-green">
      <div className="md:flex items-center justify-between bg-dark-green py-6 md:px-10 px-7">
        <ul className="md:flex md:items-center justify-between cursor-pointer mx-auto">
          <li className="md:ml 8 text-l text-white hover:text-gray duration-500 mr-10 ">Startsidan</li>
          <li className="md:ml 8 text-l text-white hover:text-gray duration-500 mr-10 ">Lagerbilar</li>
          <li className="md:ml 8 text-l text-white hover:text-gray duration-500 mr-10">Sälj din bil</li>
          <li className="md:ml 8 text-l text-white hover:text-gray duration-500 mr-10 ">Boka service</li>
          <li className="md:ml 8 text-l text-white hover:text-gray duration-500">Om oss</li>
        </ul>
      </div>



    </div >

  )
}

export default Navbar;
