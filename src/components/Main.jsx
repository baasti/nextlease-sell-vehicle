import React, { useState, useRef } from 'react'
import 'tw-elements';
import 'flowbite';
import DatePicker from "react-datepicker";

import { FileUpload } from 'primereact/fileupload';
import { ProgressBar } from 'primereact/progressbar';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/lara-light-indigo/theme.css';
import 'primereact/resources/primereact.css';

import { Calendar } from 'primereact/calendar';
import { addLocale } from 'primereact/api';


import "react-datepicker/dist/react-datepicker.css";


function Main() {

    // DATEPICKER REACT 
    const [startDate, setStartDate] = useState(new Date());

    //DATEPICKER PRIME REACT

    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month - 1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;
    let nextMonth = (month === 11) ? 0 : month + 1;
    let nextYear = (nextMonth === 0) ? year + 1 : year;

    const [date1, setDate1] = useState(null);


    let minDate = new Date();
    minDate.setMonth(prevMonth);
    minDate.setFullYear(prevYear);

    let maxDate = new Date();
    maxDate.setMonth(nextMonth);
    maxDate.setFullYear(nextYear);



    addLocale('es', {
        firstDayOfWeek: 1,
        dayNames: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
        dayNamesShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
        dayNamesMin: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
        monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
        monthNamesShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
        today: 'Hoy',
        clear: 'Claro'
    });



    //UPLOAD FILE 
    const [totalSize, setTotalSize] = useState(0);
    const toast = useRef(null);
    const fileUploadRef = useRef(null);

    const onTemplateSelect = (e) => {
        let _totalSize = totalSize;
        e.files.forEach(file => {
            _totalSize += file.size;
        });

        setTotalSize(_totalSize);
    }

    const onTemplateUpload = (e) => {
        let _totalSize = 0;
        e.files.forEach(file => {
            _totalSize += (file.size || 0);
        });

        setTotalSize(_totalSize);
        toast.current.show({ severity: 'info', summary: 'Success', detail: 'File Uploaded' });
    }

    const onTemplateRemove = (file, callback) => {
        setTotalSize(totalSize - file.size);
        callback();
    }

    const onTemplateClear = () => {
        setTotalSize(0);
    }


     const headerTemplate = (options) => {
        //  const { className, chooseButton, uploadButton, cancelButton } = options;
         const value = totalSize / 10000;
         const formatedValue = fileUploadRef && fileUploadRef.current ? fileUploadRef.current.formatSize(totalSize) : '0 B';

        return (
             <div className="bg-bluegreen" style={{ backgroundColor: 'transparent', display: 'flex', alignItems: 'center' }}>
                 {/* {chooseButton}
                 {uploadButton}
                 {cancelButton}
                 <ProgressBar value={value} displayValueTemplate={() => `${formatedValue} / 1 MB`} style={{ width: '300px', height: '20px', marginLeft: 'auto' }}></ProgressBar> */}
             </div>
         );
     }

    const itemTemplate = (file, props) => {
        return (
            <div className="flex align-items-center flex-wrap">
                <div className="flex align-items-center justify-center" style={{ width: '40%' }}>
                    <img alt={file.name} role="presentation" src={file.objectURL} width={120} />
                    {/* <span className="flex flex-column text-left ml-3">
                        {file.name}
                        <small>{new Date().toLocaleDateString()}</small>
                    </span> */}
                </div>
                {/* <Tag value={props.formatSize} severity="warning" className="px-3 py-2" /> */}
                <Button type="button" icon="pi pi-times" className="p-button-outlined p-button-rounded p-button-danger ml-auto" onClick={() => onTemplateRemove(file, props.onRemove)} />
            </div>
        )
    }

    const emptyTemplate = () => {
        return (
            <div className="flex align-items-center justify-center flex-column">
                <i className="pi pi-image mt-3 p-5 flex flex-col mr-2" style={{ 'fontSize': '3em', borderRadius: '50%', backgroundColor: '#51b8a4', color: 'white' }}></i>
                <span style={{ 'fontSize': '1em', 'fontFamily': 'Inter', color: 'var(--text-color-secondary)' }} className="flex flex-col justify-center my-5">Ladda upp dina bilder här</span>
            </div>
        )
    }

    const chooseOptions = { icon: 'pi pi-fw pi-images', iconOnly: true, className: 'custom-choose-btn p-button-rounded p-button-outlined' };
    const uploadOptions = { icon: 'pi pi-fw pi-cloud-upload', iconOnly: true, className: 'custom-upload-btn p-button-success p-button-rounded p-button-outlined' };
    const cancelOptions = { icon: 'pi pi-fw pi-times', iconOnly: true, className: 'custom-cancel-btn p-button-danger p-button-rounded p-button-outlined' };


    return (
        <div className="container mx-auto max-w-[1200px] mt-5 px-5">
            <section className='container mx-auto mt-12'>

                {/* HEADLINE SECTION */}

                <div className="flex justify-center text-center">
                    <div className='font-bold tracking-wide text-3xl mt-6'>Sälj din bil</div>
                </div>
                <div className="flex justify-center text-center mb-6">
                    <div className="max-w-[768px] mt-2 mb-2 md:mb-1 text-tiny ">För dig som inte har möjlighet att besöka oss så kan du sköta processen online.
                        Genom att fylla i formuläret här nedan ger vi en kostnadsfri värdering av din bil baserat på årtal, miltal och slitage.
                        När du säljer din bil till oss så kan du känna dig trygg med att vi ordnar en enkel och smidig process, där vi underlättar hela säljprocessen.</div>
                </div>


                {/* SELL YOUR CAR SECTION */}

                <form className="w-full">

                    <div className="bg-white p-2 rounded">

                        <div className="flex justify-center text-center">
                            <div className="text-container-2 flex p-4 mb-2">
                                <h1 className='px-3 tracking-wide font-bold text-2xl md:text-3xl border-b-2 p-4 border-gray-super-light'>Ditt fordon</h1>
                            </div>
                        </div>

                        <div className="flex flex-col md:flex-row justify-center">
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2" for="grid-reg-nr">
                                    Regnummer *
                                </label>
                                <input className="appearance-none' block w-full shadow-md border border-gray-super-light bg-gray-super-light rounded py-3 px-4 mb-3 leading-tight focus:outline-none 
                            focus:bg-white" id="grid-first-name" type="text" placeholder="ABC123" />
                            </div>
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-miles">
                                    Miltal (max 12000mil) *
                                </label>
                                <input className="appearance-none block w-full shadow-md border-gray-super-light bg-gray-super-light border rounded py-3 px-4 mb-3 leading-tight focus:outline-none
                             focus:bg-white focus:border-gray-500" id="grid-last-name" type="text" placeholder="6000" />
                            </div>
                        </div>



                        <div className="flex flex-col justify-center">

                            <div className="container mx-auto max-w-[648px] block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2 mt-4">Ladda upp bilder på ditt fordon här</div>
                            <FileUpload className="container mx-auto max-w-[648px] mb-10 bg-gray-super-light" ref={fileUploadRef} name="demo[]" url="https://primefaces.org/primereact/showcase/upload.php" multiple accept="image/*" maxFileSize={10000000}
                                onUpload={onTemplateUpload} onSelect={onTemplateSelect} onError={onTemplateClear} onClear={onTemplateClear}
                                headerTemplate={headerTemplate} itemTemplate={itemTemplate} emptyTemplate={emptyTemplate}
                                chooseOptions={chooseOptions} uploadOptions={uploadOptions} cancelOptions={cancelOptions} />

                        </div>


                        <div className="flex flex-col md:flex-row justify-center">
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 md:mb-16">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-phone">
                                    Eventuell extrautrustning
                                </label>
                                <textarea
                                    className="
                                form-control
                                block
                                w-full
                                px-3
                                py-1.5
                                text-base
                                font-normal
                                text-black
                                bg-gray-super-light bg-clip-padding
                                shadow-md
                                rounded
                                transition
                                ease-in-out
                                m-0
                                mb-4
                                focus:text-black focus:bg-white focus:outline-none
                            "
                                    id="EquipmentTextArea"
                                    rows="3"
                                    placeholder="Fabrikstillval & tillbehör. "
                                ></textarea>
                            </div>
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-phone">
                                    Eventuella anmärkningar
                                </label>
                                <textarea
                                    className="
                                form-control
                                block
                                w-full
                                px-3
                                py-1.5
                                text-base
                                font-normal
                                text-black
                                bg-gray-super-light bg-clip-padding
                                shadow-md
                                rounded
                                transition
                                ease-in-out
                                m-0
                                focus:text-gray-700 focus:bg-white focus:outline-none
                            "
                                    id="EquipmentTextArea"
                                    rows="3"
                                    placeholder="Skador på lack, repor, stenskott, kantstötta fälgar m.m.. "
                                ></textarea>
                            </div>

                        </div>



                        <div className="container mx-auto text-tiny md:text-base flex justify-center">

                            <div className="flex justify-center w-full xl:1/3 ml-2">
                                <div className="text-gray-dark mr-0">
                                    <div className="font-bold text-gray-dark pb-4"> Antal bilnycklar *</div>
                                    <div className="form-check">
                                        <input className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300
                             bg-white checked:bg-bluegreen checked:border-gray-super-light focus:outline-none transition duration-200
                              mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
                                        <label className="form-check-label inline-block text-gray-dark" for="flexRadioDefault1">
                                            1st
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 
                            bg-white checked:bg-bluegreen checked:border-gray-super-light focus:outline-none transition duration-200
                             mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
                                        <label className="form-check-label inline-block text-gray-dark" for="flexRadioDefault2">
                                            2st
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <div className="flex justify-left w-full md:1/3 md:ml-10">
                                <div>
                                    <div className="font-bold text-gray-dark pb-4">Hjul *</div>
                                    <div className="form-check">
                                        <input className="accent-bluegreen focus:accent-bluegreen checked:bg-bluegreen transition duration-200" type="checkbox" checked />
                                        <label className="ml-2 form-check-label inline-block text-gray-dark" for="flexCheckDefault" >
                                            Sommarhjul
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <input className="accent-bluegreen focus:accent-bluegreen checked:bg-bluegreen transition duration-200" type="checkbox" />
                                        <label className="ml-2 form-check-label inline-block text-gray-dark">
                                            Vinterhjul
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        {/* SERVICE SECTION - CLOSED FOR NOW */}


                        {/* <div className="flex justify-center text-center">
                            <div className="text-container-2 flex p-8 mb-2">
                                <h1 className='px-3 tracking-wide font-bold text-2xl border-b-2 p-4 border-gray-super-light'>Senaste service</h1>
                            </div>
                        </div>

                        <div className="flex justify-center">
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2" for="grid-miltal">
                                    Miltal vid senaste service
                                </label>
                                <input className="appearance-none' block w-full shadow-md border border-gray-super-light bg-gray-super-light rounded py-3 px-4 mb-3 leading-tight focus:outline-none 
                             focus:bg-white" id="grid-first-name" type="text" placeholder="6000" />
                            </div> */}

                            {/* // REACT DATE PICKER  */}
                            {/* <div className="w-full xl:w-1/3 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2" for="grid-datepicker">
                                    Miltal vid senaste service
                                </label>
                                <DatePicker className="appearance-none' block w-full shadow-md border border-gray-super-light bg-bluegreen rounded py-3 px-4 mb-3 leading-tight focus:outline-none 
                            focus:bg-white" id="grid-first-name" selected={startDate} onChange={(date) => setStartDate(date)} />
                            </div> */}

                            {/* // PRIMEREACT DATE PICKER  DENNA SOM SKA ANVÄNDAS   */}
                            {/* <div className="w-full xl:w-1/3 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark font-bold mb-2" for="grid-datepicker" htmlFor="basic">Datum senaste service</label>
                                <Calendar className="appearance-none' w-full leading-tight focus:outline-none 
                            focus:bg-white" id="basic" value={date1} onChange={(e) => setDate1(e.value)} />
                            </div> */}
                        {/* </div> */}




                        {/* CUSTOMER INFO SECTION */}

                        <div className="flex justify-center text-center">
                            <div className="text-container-2 flex p-8 mt-2 mb-2">
                                <h1 className='px-3 tracking-wide font-bold text-2xl md:text-3xl border-b-2 p-4 border-gray-super-light'>Dina uppgifter</h1>
                            </div>
                        </div>

                        <div className="flex flex-col md:flex-row justify-center">
                            <div class="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-email">
                                    Förnamn *
                                </label>
                                <input className="appearance-none block w-full shadow-md border-gray-super-light bg-gray-super-light border rounded py-3 px-4 mb-3 leading-tight focus:outline-none
                             focus:bg-white focus:border-gray-500" id="grid-email" type="text" placeholder="Förnamn" />
                            </div>
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-email">
                                    Efternamn *
                                </label>
                                <input className="appearance-none block w-full shadow-md border-gray-super-light bg-gray-super-light border rounded py-3 px-4 mb-3 leading-tight focus:outline-none
                             focus:bg-white focus:border-gray-500" id="grid-email" type="text" placeholder="Efternamn" />
                            </div>
                        </div>

                        <div className="flex flex-col md:flex-row justify-center">
                            <div class="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-email">
                                    E-postadress *
                                </label>
                                <input className="appearance-none block w-full shadow-md border-gray-super-light bg-gray-super-light border rounded py-3 px-4 mb-3 leading-tight focus:outline-none
                             focus:bg-white focus:border-gray-500" id="grid-email" type="email" placeholder="mail@mail.com" />
                            </div>
                            <div className="w-full xl:w-1/3 md:w-1/2 px-3 mb-6 md:mb-1">
                                <label className="block uppercase tracking-wide text-xs text-gray-dark  font-bold mb-2" for="grid-phone">
                                    Telefonnummer *
                                </label>
                                <input className="appearance-none block w-full shadow-md border-gray-super-light bg-gray-super-light border rounded py-3 px-4 mb-3 leading-tight focus:outline-none
                             focus:bg-white focus:border-gray-500" id="grid-phone" type="text" placeholder="+46700000000" />
                            </div>
                        </div>


                        {/* CONFIRM AND SEND SECTION */}

                        <div className="flex justify-center md:p-8 md:mt-6 mb-6">
                            <label className="flex items-center max-w-[668px] mx-auto">
                                <input className="ml-4 accent-bluegreen focus:accent-bluegreen checked:bg-bluegreen" type="checkbox" class="accent-bluegreen focus:accent-bluegreen checked:bg-bluegreen" />
                                <span className="ml-4 text-tiny md:text-base">Jag godkänner att mina personuppgifter hanteras enligt <span className="underline cursor-pointer">integritetspolicyn</span></span>
                            </label>
                        </div>
                        <div className="button-container flex justify-center p-1">
                            <button className="bg-bluegreen shadow-md hover:bg-bluegreen hover:opacity-95 text-tiny md:text-base md:font-base text-white py-4 px-8 md:px-16 rounded">
                                Skicka för värdering
                            </button>
                        </div>
                    </div>
                </form>

                {/* Q&A SECTION */}

                <div className="flex justify-center text-center">
                    <div className="text-container-2 flex p-8 mt-6 mb-6">
                        <h1 className='px-3 font-bold tracking-wide text-2xl md:text-3xl border-b-2 p-4 border-gray-super-light'>Frågor & svar</h1>
                    </div>
                </div>


                <div className="w-full flex flex-col justify-center items-center md:flex-row px-8">
                    <div className='flex flex-col xl:w-1/3 sm:w-full px-3 py-3  '>
                        <h1 className='font-bold tracking-wide text-lg'>Köper ni in alla bilar? </h1>
                        <p className="mt-2 text-tiny text-gray-dark ">Vi köper din bil oavsett märke eller modell. Däremot jobbar vi främst med bilar som max är 10 år gamla och gått mindre än 12 000 mil.</p>
                    </div>
                    <div className='flex flex-col xl:w-1/3 sm:w-full px-3 py-3 '>
                        <h1 className='font-bold tracking-wide text-lg'>Löser ni lånet på min bil?  </h1>
                        <p className="mt-2 text-tiny text-gray-dark ">Ja, vi löser eventuella lån på din bil samt att vi betalar ut resterande belopp direkt till ditt konto. Pengarna ser du redan samma dag.</p>
                    </div>
                    <div className='flex flex-col xl:w-1/3 sm:w-full px-3 py-3  '>
                        <h1 className='font-bold tracking-wide text-lg'>Löser ni min leasing?</h1>
                        <p className="mt-2 text-tiny text-gray-dark ">Ja, vi löser din leasing av både din privat- eller tjänstebil. Olika banker har olika villkor för lösen av din bil i förtid. Kontakta oss så hjälper vi dig.</p>
                    </div>
                </div>

                <div className="container mx-auto flex justify-center mt-8 mb-8">
                    <div className="md:p-10 md:w-4/6 justify-left rounded-lg bg-bluegreen shadow-md md:m-5">
                        <div className="p-6 flex flex-col justify-center">
                            <div className="text-white md:text-3xl font-base mb-2 justify-center flex py-2 md:p-3 border-b-2 border-white-light">Hur vi bedömer bilens värde</div>
                            <div className="text-white flex flex-col justify-center text-sm md:text-tiny">
                                <div className="md:text-lg text-white font-bold md:p-3 mb-4">Vid en bilvärdering tar vi hänsyn till följande: </div>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Aktuella försäljningspriser.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Miltal. Hur många mil bilen har körts.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Extrautrustning. Såsom dragkrok, värmare etc.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Fordons- och besiktningshistorik.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Bilens allmänna skick. Såsom rost, repor, kvalitén på fälgarna och däcken.</li>
                            </div>
                        </div>

                        <div className=" flex flex-col p-6 justify-center">
                            <div className="text-white md:text-3xl font-base mb-2 justify-center flex py-2 md:p-3 border-b-2 border-white-light">Fördelar med att sälja eller byta in bilen hos oss</div>
                            <div className="text-white flex flex-col justify-center md:text-tiny mb-4">
                                <div className="md:text-lg text-white font-bold text-sm md:p-3 mb-4">Sälja sin bil privat kan vara en tidsödande och krävande process,
                                    och det kan vara svårt att hålla koll på alla detaljer som man bör ha i åtanke innan försäljningen går igenom. </div>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Helt kostnadsfritt att värdera din bil.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Enkelt, smidigt och bekvämt. Vi sköter det administrativa och du slipper allt krångel</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Snabb och trygg affär. Generellt så sker affären snabbt och vi säkerställer att du har pengarna på kontot i rätt tid.</li>
                                <li className="md:p-1 text-xs md:text-base text-white-md">Kunskap, expertis och personlig service. Värderingen sker alltid av en expert hos oss som har många års erfarenhet av bilförsäljning. Du får en korrekt bilvärdering i förhållande till bilmarknaden.</li>


                            </div>
                        </div>
                    </div>


                </div>


            </section>
        </div>
    )
}

export default Main;
