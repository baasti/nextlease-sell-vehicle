import Navbar from './components/Navbar.jsx'
import Hero from './components/Hero.jsx';
import Main from './components/Main.jsx';
import './App.css';




function App() {


  return (
    <div className="bg-white w-full h-screen">
      <Navbar />
      <Hero />
      <Main />


    </div>
  );
}

export default App;
